/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:
const getAverage = (first,second,third,fourth) => {
	let totalGrade = first + second + third + fourth;
	let average = (totalGrade/4);

	if (average <= 74){
		console.log (`Your quarterly average is ${average}. You have received a failed mark`);
	} else if (average >=75 && average <= 80){
		console.log (`Congratulations! Your quarterly average is ${average}. You have received a Beginner mark`);
	} else if (average >= 81 && average <= 85){
		console.log (`Congratulations! Your quarterly average is ${average}. You have received a Developing mark`);
	} else if (average >=86 && average <= 90){
		console.log(`Congratulations! Your quarterly average is ${average}. You have received a Above Average mark`);
	}else if (average >=91){
		console.log(`Congratulations! Your quarterly average is ${average}. You have received a Advance mark`);
	}
}

console.log('Your Grades are 80, 96, 43, 75')
getAverage(80,96,43,75);

console.log('Your Grades are 81, 80, 93, 75')
getAverage(81,80,93,75);

console.log('Your Grades are 80, 96, 43, 75')
getAverage(80,96,86,90);


/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here: 

for (let x = 1; x <= 20; x++){
	if(x % 2 == 0){
		console.log(`${x} is even.`)
	} else {
		console.log(`${x} is odd.`)
	}
	if (x > 15){
		break;
	}
}

/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

let hero = JSON.stringify({
	heroName: prompt('Enter hero Name'),
	origin: "Minoan Empire",
	description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.",
	skills: {
		skill1: "Soul Steal",
		skill2: "Explosion",
		skill3: "Chase Fate"
	}
})

console.log(hero)

